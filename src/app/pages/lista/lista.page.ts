import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {
nome: string;
tel: string;
email: string;
public ficha: any[]=[
{nome: 'Wellington',tel: '1121859200',email: 'w@g'},
{nome: 'Camille',tel: '1198547544',email: 'c@g'},
{nome: 'Priscila',tel: '1198563252',email: 'p@g'},
{nome: 'Gabriela',tel: '1155454758',email: 'g@g'}]
  constructor(private navControl: NavController, private toasCtrl: ToastController) { }

  async mostraDados(){
    let nova = {nome: this.nome, tel: this.tel, email: this.email}
    this.ficha.push(nova)
    //console.log(this.ficha);  
    //this.navControl.push('listagem',{data:this.ficha});
    const toast = await this.toasCtrl.create({
      message:"Dados do "+ this.nome +" gravados com sucesso",
      duration:3000,
      position: 'middle',
      color:'primary'
    });
    toast.present();

  }
  ngOnInit() {
  }

}
