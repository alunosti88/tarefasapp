import { Component } from '@angular/core';
import { AlertController, ToastController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
tarefas: any[] = [];
  constructor(
    private alertaControl:AlertController,
    private toastControl: ToastController,
    private actionSheetControl: ActionSheetController
    ){// os comandos desta sessão são executados durante o load da página
      let tarefasJason = localStorage.getItem('tarefaDb');
      //debugger;
      if(tarefasJason != null)
      {
        this.tarefas = JSON.parse(tarefasJason);
      }
    }

async adicionarTarefa(){
  const alerta = await this.alertaControl.create({
    header:'O que precisa fazer?',
    inputs: [
      {name: 'nova',type: 'text', placeholder: 'o que você precisa fazer?' }
    ],
    buttons:[
      {text:'Cancelar',role:'cancel', cssClass:"secondary", 
      handler: ()=>{
        // caso o usuário clique em cancelar???
        console.log('Acho que você clicou em cancelar?!?!?!');
      }}
    ,{
      text: 'Ok', 
      handler: (form) =>{
       //debugger; 
       this.addTarefa(form);
      }
    }
  ]
  });
  await alerta.present();
}
 async addTarefa(nova: any) {//>>>>>>>>>
  //verifica se usuário digitou uma tarefa
  if (nova.trim().length < 1){
    const toast = await this.toastControl.create({
      message: 'Informe o que precisa fazer',
      duration: 2000,
      position: 'middle',
      color:'primary'
    });  
    toast.present();
    return;
  }
  let tarefa = { nome: nova, feito: false};
  //debugger;
  this.tarefas.push(tarefa);
  this.updateLocalStorage();
}//final do método addTarefa <<<<<<<<<<<<<<<<<<<

updateLocalStorage(){
  localStorage.setItem('tarefaDb',JSON.stringify(this.tarefas));
}

excluir(tarefinha: any){
  this.tarefas = this.tarefas.filter(a => tarefinha != a); // expressão Lambda
  this.updateLocalStorage();
}
async abrirAcoes(tarefinha:any){

    const actionsheet = await this.actionSheetControl.create({
      header: "O que deseja fazer?",
      buttons: [{
        text: tarefinha.feito ? 'Desmarcar' : 'Marcar',
        icon: tarefinha.feito ? 'radio-button-off' : 'checkmark-circle',
        handler:()=>{
          tarefinha.feito = !tarefinha.feito; // inverte o valor de task
          this.updateLocalStorage();
        }
       },
     {
       text: 'Cancelar',
       icon: 'close',
       role: 'cancel',
       handler: () => {
         console.log('clicou em cancelar');
       }
     }]
    });
    await actionsheet.present();// ecexutar a actionsheet
  }// final do actionsheet
}

